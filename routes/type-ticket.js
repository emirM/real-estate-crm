var express = require('express');
var router = express.Router();
var csrf = require('csurf');

var TypeTicket = require('../models/type-ticket');
var ObjectId = require('mongoose').Types.ObjectId;

var csrfPrtoection = csrf();
router.use(csrfPrtoection);

router.get('/', isLoggedIn,function(req, res, next) {
    var errorMessages = "";

    TypeTicket.find(function(err, typeTickets) {
        if (err) {
            errorMessages+=err;
            return res.render('type-tickets/index', {
                csrfToken: req.csrfToken(),
                errorMessages: errorMessages
            });
        }
        if (!typeTickets || typeTickets.length==0) {
            return res.render('type-tickets/index',  {
                csrfToken: req.csrfToken()
            });
        }

        res.render('type-tickets/index',  {
            csrfToken: req.csrfToken(),
            typeTickets: typeTickets
        });
    });
});

router.post('/create', function(req, res, next) {
    req.checkBody('name', '"Наименование"-обьязательное поле.').notEmpty();//проверяем не пустое ли это поле
    var errors = req.validationErrors();

    if (errors) { //если есть ошибки
        var messages='';
        errors.forEach(function (error) {
            messages += error.msg+' ';
        });
        return res.status(400).json({errorMessages: messages});//то сообщаем об этом
    }

    TypeTicket.findOne({name: req.body.name}, function(err, typeTicket) {//ищем существует ли в базе Тип клиента с таким именем
        if (err) {//если вышла ошибка при поиске, то вернем это сообщ.
            return res.status(500).json({
                errorMessages: 'Что-то пошла не так. Пробуйте еще раз пж. Ошибка: '+err
            });
        }
        if(!typeTicket) {
            var newTypeTicket = new TypeTicket(); //создаем экземпляр
            newTypeTicket.name = req.body.name;
            if (ObjectId.isValid(req.body.selectionTypeTicket)) {
                newTypeTicket.selectionTypeTicketName = req.body.selectionTypeTicketName;
                newTypeTicket.selectionTypeTicket = req.body.selectionTypeTicket;
            }

            newTypeTicket.save(function(err) {
                if (err) { //если ошибка при сохр то вернем его
                    return res.status(500).json({errorMessages: err});
                }
                TypeTicket.findOne({name: newTypeTicket.name}, function(err, typeTicket) { // если успешно сохранился то находим его в бд, чтобы вернут его _id
                    if (err) {
                        return res.status(500).json({errorMessages: err});
                    }

                    res.status(201).json({typeTicket: typeTicket});
                });
            });
        } else {
            res.status(400).json({errorMessages: 'Тип с таким именем уже существует.'});
        }
    });
});

router.post('/edit', function (req, res, next) {
    var typeTickeId = req.body._id;
    if (!typeTickeId || !ObjectId.isValid(typeTickeId)) {//если пустое id либо формат неправильный
        return res.status(400).json({errorMessages: 'Что-то пошло не так! данные неправильного формата.'});
    }

    TypeTicket.findOne({name: req.body.name}, function(err, typeTicket) {//ищем существует ли в базе Тип клиента с таким именем
        if (err) {//если вышла ошибка при поиске, то вернем это сообщ.
            return res.status(500).json({errorMessages: 'Что-то пошла не так. Пробуйте еще раз пж. Ошибка: '+err});
        }

        if(typeTicket._id == typeTickeId || !typeTicket) {
            TypeTicket.findById(typeTickeId, function(err, typeTicket) { //находим ы БД данный элемент
                if (err) { //если ошибка вернем его
                    return res.status(500).json({errorMessages: err});
                }
                if(!typeTicket) { //если пустое тогда говорим что не найдено
                    return res.status(404).json({errorMessages: 'Не найдено.'});
                }

                typeTicket.name = req.body.name;
                if (ObjectId.isValid(req.body.selectionTypeTicket)) {
                    typeTicket.selectionTypeTicketName = req.body.selectionTypeTicketName;
                    typeTicket.selectionTypeTicket = req.body.selectionTypeTicket;
                }
                typeTicket.save(function(err) {//сохраняем
                    if (err) { //если есть ошибка то вернем его
                        return res.status(500).json({errorMessages: err});
                    }
                    res.status(200).json({typeTicket: typeTicket});
                });
            });
        } else {
            res.status(400).json({errorMessages: 'Тип с таким именем уже существует.'});
        }
    });

});

router.delete('/delete', function(req, res, next) {
    var typeTicketId = req.body._id;
    if (!typeTicketId || !ObjectId.isValid(typeTicketId)) return res.status(400).json({errorMessages: 'Что-то пошло не так.'});

    TypeTicket.findById(typeTicketId, function(err, typeTicket) {
        if (err) {
            return res.status(500).json({errorMessages: err});
        }
        if (!typeTicket) {
            return res.status(404).json({errorMessages: "Не найдено."});
        }
        typeTicket.remove(function(err) {
            if (err) {
                return res.status(500).json({errorMessages: err});
            }
            res.status(200).json({msg: "success"});
        });
    });
});

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/users/signin');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}