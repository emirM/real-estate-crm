var express = require('express')
var router = express.Router()
var csrf = require('csurf')
var ObjectId = require('mongoose').Types.ObjectId

var Ticket = require('../models/ticket')
var Client = require('../models/client')
var TypeClient = require('../models/type-client')
var CategoryObject = require('../models/category-object')
var TypeTicket = require('../models/type-ticket')

var csrfProtestion = csrf()
router.use(csrfProtestion)

router.get('/', isLoggedIn, function(req, res, next) {//вывод страницы
    Ticket.find(function(err, tickets) {
        TypeClient.find(function(err, typeClients) {
            Client.find(function(err, clients) {
                CategoryObject.find(function(err, categoriesObject) {
                    TypeTicket.find(function(err, typeTickets) {
                        res.render('tickets/index', {
                            csrfToken: req.csrfToken(),
                            errorMsg: err,
                            tickets: tickets,
                            typeClients: typeClients,
                            categoriesObject: categoriesObject,
                            typeTickets: typeTickets,
                            clients: clients
                        })
                    })
                })
            })
        })
    })
})

router.post('/create', function(req, res, next) {//create создание
    var ticket = new Ticket({
        type_ticket: ObjectId.isValid(req.body.type_ticket)?req.body.type_ticket:null,
        type_ticketName: req.body.type_ticketName,
        categoryObject: ObjectId.isValid(req.body.categoryObject)?req.body.categoryObject:null,
        categoryObjectName: req.body.categoryObjectName,
        client: ObjectId.isValid(req.body.client)?req.body.client:null,
        parameters: JSON.parse(req.body.parameters),
        user_created: ObjectId(req.body.user_created)?req.body.user_created:null,
        prioritet: req.body.prioritet,
        date_create: Date.now()
    })

    ticket.save(function(err) {
        if (err) {
            return res.status(500).json({errorMessages: err})
        }
        Ticket.findOne({date_create: ticket.date_create}, function(err, ticket) {
            if (err) {
                return res.status(500).json({errorMessages: err})
            }

            res.status(201).json({ticket: ticket})
        })
    })
})

router.post('/edit', function(req, res, next) {
    var ticketId = req.body._id
    if(!ticketId) return res.status(403).json({status: false, message: 'Неправильный запрос'})

    Ticket.findById(ticketId, function(err, ticket) {
        if(err) {
            return res.status(500).json({status: false, message: err})
        }
        ticket.type_ticket = ObjectId.isValid(req.body.type_ticket)?req.body.type_ticket:null
        ticket.type_ticketName = req.body.type_ticketName
        ticket.categoryObject = ObjectId.isValid(req.body.categoryObject)?req.body.categoryObject:null
        ticket.categoryObjectName = req.body.categoryObjectName
        ticket.parameters = JSON.parse(req.body.parameters)
        ticket.status_ticket = ObjectId.isValid(req.body.status_ticket)?req.body.status_ticket:null
        ticket.user_created = ObjectId(req.body.user_created)?req.body.user_created:null
        ticket.prioritet = req.body.prioritet
        ticket.client = req.body.client

        ticket.save(function(err) {
            if(err) {
                return res.status(500).json({status: false, message: err})
            }
            res.status(200).json({status: true, message: "Успешно изменена", ticket: ticket})
        })
    })
})

router.delete('/delete', function(req, res, next) {
    var ticketId = req.body._id
    if (!ticketId || !ObjectId.isValid(ticketId)) return res.status(400).json({errorMessages: 'Что-то пошло не так.'})

    Ticket.findById(ticketId, function(err, ticket) {
        if(err) {
            return res.status(500).json({errorMessages: err})
        } 
        if (!ticket) {
            return res.status(404).json({errorMessages: "Не найдено."})
        }
        ticket.remove(function(err) {
            if (err) {
                return res.status(500).json({errorMessages: err})
            }
            res.status(200).json({errorMessages: "success"})
        })
    })
})

router.post('/selection/', function (req, res, next) {
    var type_ticket = req.body.type_ticket
    var categoryObject = req.body.categoryObject
    if(!ObjectId.isValid(type_ticket) || !ObjectId.isValid(categoryObject)) return res.status(400).json({errorMessages: "Данные непправильного формата."})

    var parameters = JSON.parse(req.body.parameters)
    
    var newTickets = []
    Ticket.find({
        type_ticket: type_ticket,
        categoryObject: categoryObject, 
        parameters: {$elemMatch: {name:parameters[0].name, value:parameters[0].value}}
    }, function(err, tickets) {

        if(tickets.length==0) return res.status(200).json({tickets:[]})
        var count=0
        for(var j=0; j<tickets.length; j++) {
            for(var k=0; k<tickets[j].parameters.length; k++) {
                var parameterTicketK = tickets[j].parameters[k]
                for(var i=0; i<parameters.length; i++) {
                    if (parameters[i].name == parameterTicketK.name && parameters[i].value == parameterTicketK.value) {
                        count++
                    }
                }
            }

            if(count == parameters.length) {
                newTickets.push(tickets[j])
            }
            count = 0
        }

        res.status(200).json({tickets: newTickets})
    })
})

module.exports = router

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/users/signin');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}