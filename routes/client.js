var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var ObjectId = require('mongoose').Types.ObjectId;

var Client = require('../models/client');
var TypeClient = require('../models/type-client');
var csrfPrtoection = csrf();
router.use(csrfPrtoection);

router.get('/', isLoggedIn, function(req, res, next) {
    var successMsg = '';
    Client.find(function(err, clients) {
        return res.render('clients/index',
            {
                csrfToken: req.csrfToken(),
                title: 'Клиенты',
                clients: clients,
                successMsg: successMsg,
                noMessages: !successMsg
            });
    });
});

router.get('/create', function(req, res, next) {

    TypeClient.find(function(err, typeClients) {
        if (err) {
            res.render('clients/create',
                {
                    csrfToken: req.csrfToken(),
                    errorMessages: err,
                    typeClients: typeClients
                });
        }
        return res.render('clients/create',
            {
                csrfToken: req.csrfToken(),
                errorMsg: '',
                typeClients: typeClients
            });
    });

});

router.post('/create', function(req, res, next) {
    req.checkBody('first_name', '"Имя" не должна быть пустым.').notEmpty();
    req.checkBody('last_name', '"Фамилия" не должна быть пустым.').notEmpty();
    req.checkBody('mobile_number1', '"Мобльный номер 1" не должна быть пустым.').notEmpty();
    var errors = req.validationErrors();

    var client = new Client({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        mobile_number1: req.body.mobile_number1,
        mobile_number2: req.body.mobile_number2,
        email: req.body.email,
        gender: req.body.gender,
        address: req.body.address,
        facebook: req.body.facebook,
        instagram: req.body.instagram,
        type_client: ObjectId.isValid(req.body.type_client)?req.body.type_client:null
    });

    if (errors) {
        var messages = '';
        errors.forEach(function(error) {
            messages+=error.msg;
        });
        TypeClient.find(function(err, typeClients) {
            for(var i=0; i<typeClients.length; i++) {
                if(typeClients[i]._id.toString()==client.type_client.toString()) {
                    typeClients[i].selected = true;
                }
            }

            if (err) {
                messages+=' '+err;
                return res.render('clients/create',
                    {
                        csrfToken: req.csrfToken(),
                        errorMessages: messages,
                        typeClients: typeClients,
                        client: req.body
                    });
            }

            return res.render('clients/create', {
                csrfToken: req.csrfToken(),
                typeClients: typeClients,
                client: req.body,
                errorMessages: messages
            });
        });

    } else {
        var birthdaydate;
        if(req.body.birthday) {//25.12.2016 13:43
            var dateTimeArr = req.body.birthday.split(' ');//['25.12.2016', '13:43'];
            var date = dateTimeArr[0].split('.');//['25','12','2016']
            var time = dateTimeArr[1].split(':');//['13','43']
            birthdaydate = new Date(date[0], date[1], date[2], time[0], time[1]);
        }
        client.birthday = birthdaydate;
        client.save(function(err) {
            if(err) {
                var messages = err;
                TypeClient.find(function(err, typeClients) {
                    for(var i=0; i<typeClients.length; i++) {
                        if(typeClients[i]._id.toString()==client.type_client.toString()) {
                            typeClients[i].selected = true;
                        }
                    }
                    if (err) {
                        messages+=' '+err;
                        return res.render('clients/create',
                            {
                                csrfToken: req.csrfToken(),
                                errorMessages: messages,
                                typeClients: typeClients,
                                client: client
                            });
                    }

                    return res.render('clients/create', {
                        csrfToken: req.csrfToken(),
                        typeClients: typeClients,
                        client: client,
                        errorMessages: err
                    });
                });
            } else {
                res.redirect('/clients/');
            }
        });
    }

});

router.get('/edit/:id', function(req, res, next) {
    var clientId = req.params.id;
    var messages='';
    Client.findById(clientId, function(err, client) {
        if(err) {
            return res.redirect('/clients/');
        } else {
            TypeClient.find(function(err, typeClients) {
                for(var i=0; i<typeClients.length; i++) {
                    if(client.type_client && typeClients[i]._id.toString()==client.type_client.toString()) {
                        typeClients[i].selected = true;
                    }
                }

                if (err) {
                    messages+=' '+err;
                    return res.render('clients/edit',
                        {
                            csrfToken: req.csrfToken(),
                            errorMessages: messages,
                            typeClients: typeClients,
                            client: client
                        });
                }

                return res.render('clients/edit', {
                    csrfToken: req.csrfToken(),
                    typeClients: typeClients,
                    client: client
                });
            });
        }
    });
});

router.post('/edit', function(req, res, next) {
    var clientId = req.body._id;
    var birthdaydate;

    req.checkBody('first_name', '"Имя" не должна быть пустым.').notEmpty();
    req.checkBody('last_name', '"Фамилия" не должна быть пустым.').notEmpty();
    req.checkBody('mobile_number1', '"Мобльный номер 1" не должна быть пустым.').notEmpty();
    var errors = req.validationErrors();

    var messages = '';
    if (errors) {
        errors.forEach(function(error) {
            messages+=error.msg;
        });
        TypeClient.find(function(err, typeClients) {
            for(var i=0; i<typeClients.length; i++) {
                if(req.body.type_client && typeClients[i]._id.toString()==req.body.type_client.toString()) {
                    typeClients[i].selected = true;
                }
            }

            if (err) {
                messages+=' '+err;
                return res.render('clients/edit',
                    {
                        csrfToken: req.csrfToken(),
                        errorMessages: messages,
                        typeClients: typeClients,
                        client: req.body
                    });
            }

            return res.render('clients/edit', {
                csrfToken: req.csrfToken(),
                typeClients: typeClients,
                client: req.body,
                errorMessages: messages
            });
        });

    } else {
        if(req.body.birthday) {//25.12.2016 13:43
            var dateTimeArr = req.body.birthday.split(' ');//['25.12.2016', '13:43'];
            var date = dateTimeArr[0].split('.');//['25','12','2016']
            var time = dateTimeArr[1].split(':');//['13','43']
            birthdaydate = new Date(date[0], date[1], date[2], time[0], time[1]);
        }
        Client.findById(clientId, function(err, client) {
            if(err) {
                messages+=' '+err;
                TypeClient.find(function(err, typeClients) {
                    for(var i=0; i<typeClients.length; i++) {
                        if(client.type_client && typeClients[i]._id.toString()==client.type_client.toString()) {
                            typeClients[i].selected = true;
                        }
                    }

                    if (err) {
                        messages+=' '+err;
                        return res.render('clients/edit',
                            {
                                csrfToken: req.csrfToken(),
                                errorMessages: messages,
                                typeClients: typeClients,
                                client: req.body
                            });
                    }

                    return res.render('clients/edit', {
                        csrfToken: req.csrfToken(),
                        typeClients: typeClients,
                        client: req.body,
                        errorMessages: messages
                    });
                });
            } else {
                if(req.body.birthday) {//25.12.2016 13:43
                    var dateTimeArr = req.body.birthday.split(' ');//['25.12.2016', '13:43'];
                    var date = dateTimeArr[0].split('.');//['25','12','2016']
                    var time = dateTimeArr[1].split(':');//['13','43']
                    birthdaydate = new Date(date[0], date[1], date[2], time[0], time[1]);
                }
                client.first_name = req.body.first_name;
                client.last_name = req.body.last_name;
                client.mobile_number1 = req.body.mobile_number1;
                client.mobile_number2 = req.body.mobile_number2;
                client.email = req.body.email;
                client.gender = req.body.gender;
                client.birthday = birthdaydate;
                client.address = req.body.address;
                client.facebook = req.body.facebook;
                client.instagram = req.body.instagram;
                client.type_client = ObjectId.isValid(req.body.type_client)?req.body.type_client:null;

                client.save(function(err) {
                    if(err) {
                        TypeClient.find(function(err, typeClients) {
                            for(var i=0; i<typeClients.length; i++) {
                                if(client.type_client && typeClients[i]._id.toString()==client.type_client.toString()) {
                                    typeClients[i].selected = true;
                                }
                            }

                            if (err) {
                                messages+=' '+err;
                                return res.render('clients/edit',
                                    {
                                        csrfToken: req.csrfToken(),
                                        errorMessages: messages,
                                        typeClients: typeClients,
                                        client: req.body
                                    });
                            }

                            return res.render('clients/edit', {
                                csrfToken: req.csrfToken(),
                                typeClients: typeClients,
                                client: req.body,
                                errorMessages: messages
                            });
                        });
                    } else {
                        res.redirect('/clients/');
                    }
                });
            }
        });
    }
});

router.get('/delete/:id', function(req, res, next) {
    var clientId = req.params.id;
    if(!clientId) return res.redirect('/clients/');

    Client.findById(clientId, function(err, client) {
        if(err) {
            res.redirect('/clients/');
        } else {
            client.remove(function(err) {
                if (err) {
                    console.log(err);
                }
            });
        }
    });
    res.redirect('/clients/');
});


router.post('/api/create', function(req, res, next) {
    req.checkBody('first_name', '"Имя" не должна быть пустым.').notEmpty();
    req.checkBody('last_name', '"Фамилия" не должна быть пустым.').notEmpty();
    req.checkBody('mobile_number1', '"Мобльный номер 1" не должна быть пустым.').notEmpty();
    var errors = req.validationErrors();

    var client = new Client({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        mobile_number1: req.body.mobile_number1,
        mobile_number2: req.body.mobile_number2,
        email: req.body.email,
        gender: req.body.gender,
        address: req.body.address,
        facebook: req.body.facebook,
        instagram: req.body.instagram,
        type_client: ObjectId.isValid(req.body.type_client)?req.body.type_client:null
    });

    if (errors) {
        var messages = '';
        errors.forEach(function(error) {
            messages+=error.msg;
        });
        TypeClient.find(function(err, typeClients) {
            for(var i=0; i<typeClients.length; i++) {
                if(typeClients[i]._id.toString()==client.type_client.toString()) {
                    typeClients[i].selected = true;
                }
            }

            if (err) {
                messages+=' '+err;
                return res.json({
                        errorMessages: messages
                    });
            }

            res.json({
                errorMessages: messages
            });
        });

    } else {
        var birthdaydate;
        if(req.body.birthday) {//25.12.2016 13:43
            var dateTimeArr = req.body.birthday.split(' ');//['25.12.2016', '13:43'];
            var date = dateTimeArr[0].split('.');//['25','12','2016']
            var time = dateTimeArr[1].split(':');//['13','43']
            birthdaydate = new Date(date[0], date[1], date[2], time[0], time[1]);
        }
        client.birthday = birthdaydate;
        client.save(function(err) {
            if(err) {
                var messages = err;
                res.json({
                    errorMessages: messages,
                    client: client
                });
            }
            res.json({
                client: client
            });
        });
    }

});

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/users/signin');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}