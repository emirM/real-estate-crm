var express = require('express');
var router = express.Router();
var csrf = require('csurf');

var ObjectId = require('mongoose').Types.ObjectId;
var CategoryObject = require('../models/category-object');

var csrfProtection = csrf();
router.use(csrfProtection);

router.get('/', isLoggedIn, function(req, res, next) {
    CategoryObject.find(function(err, categoriesObject) {
        if (err) {
            return res.render('categories-object/index', {csrfToken: req.csrfToken(), errorMessages: err});
        }
        res.render('categories-object/index', {csrfToken: req.csrfToken(), categoriesObject: categoriesObject});
    });
});

router.post('/create', function(req, res, next) {
    req.checkBody("name", '"Наименование"-обьязательное поле.').notEmpty();//проверка на пустоту поле name
    var errors = req.validationErrors();
    if (errors) {
        var messages = "";
        errors.forEach(function(error) {
            messages += error.msg;
        });
        return res.status(400).json({
            errorMessages: messages
        });
    }

    CategoryObject.findOne({name: req.body.name}, function(err, categoryObject) { //ищем категорию с таким именем
        if (err) {
            return res.status(500).json({
                errorMessages: "Что-то пошла не так. Пожалуйста попробуйте еще раз. Ошибка: "+err
            });
        }
        if (categoryObject) { //если уже есть с таким именем то выводим сообщение
            return res.status(400).json({errorMessages: "Категория объект с таким наименованием уже существует. Пожалуйста введите другое название."})
        }
        var newCategoryObject = new CategoryObject({
            name: req.body.name,
            parameters: JSON.parse(req.body.parameters)
        });
        newCategoryObject.save(function (err) { //сохраняем в БД
            if (err) {
                return res.status(500).json({errorMessages: "Ошибка, при созданий объекта. Детали: "+err});
            }
            CategoryObject.findOne({name: newCategoryObject.name}, function(err, categoryObject) { //вытаскиваем с базы только что созданную категорию
                if (err) {
                    return res.status(400).json({errorMessages: "Что-пошла не так. Пожалуйста попробуйте еще раз. Ошибка: "+err});
                }

                res.status(201).json({categoryObject: categoryObject});
            });
        });
    });

});

router.post('/edit', function (req, res, next) {
    var categoryObjectId = req.body._id;
    if (!categoryObjectId || !ObjectId.isValid(categoryObjectId)) {//если пустое id либо формат неправильный
        return res.status(400).json({errorMessages: 'Что-то пошло не так!'});
    }

    CategoryObject.findById(categoryObjectId, function(err, categoryObject) { //находим ы БД данный элемент
        if (err) { //если ошибка вернем его
            return res.status(500).json({errorMessages: err});
        }
        if(!categoryObject) { //если пустое тогда говорим что не найдено
            return res.status(404).json({errorMessages: 'Не найдено.'});
        }

        categoryObject.name = req.body.name;
        categoryObject.parameters = JSON.parse(req.body.parameters);
        categoryObject.save(function(err) {//сохраняем
            if (err) { //если есть ошибка то вернем его
                return res.status(500).json({errorMessages: err});
            }
            res.status(200).json({categoryObject: categoryObject});
        });
    });

});

router.delete('/delete', function(req, res, next) {
    var categoryObjectId = req.body._id;
    if (!categoryObjectId || !ObjectId.isValid(categoryObjectId)) return res.status(400).json({errorMessages: 'Что-то пошло не так.'});

    CategoryObject.findById(categoryObjectId, function(err, categoryObject) {
        if (err) {
            return res.status(500).json({errorMessages: err});
        }
        if (!categoryObject) {
            return res.status(404).json({errorMessages: "Не найдено."});
        }

        categoryObject.remove(function(err) {
            if (err) {
                return res.status(500).json({errorMessages: err});
            }
            res.status(200).json({errorMessages: "success"});
        });
    });
});

module.exports = router;


function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/users/signin');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}