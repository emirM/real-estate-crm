var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var TypeClient = require('../models/type-client');
var ObjectId = require('mongoose').Types.ObjectId;

var csrfPrtoection = csrf();
router.use(csrfPrtoection);

router.get('/', isLoggedIn, function(req, res, next) {
    return index(res, req, null, null, null);
});

router.get('/create', function(req, res, next) {
    var errorMessages = req.flash('error');
    return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: errorMessages, typeClient:null});
});

router.post('/create', function(req, res, next) {
    req.checkBody('name', '"Наименование"-обьязательное поле.').notEmpty();//проверяем не пустое ли это поле
    var errors = req.validationErrors();
    console.log(errors);
    if (errors) { //если есть ошибки
        var messages='';
        errors.forEach(function (error) {
            messages+=error.msg+' ';
        });
        return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: messages, typClient: null});//то сообщаем об этом
    }

    TypeClient.findOne({name: req.body.name}, function(err, typeClient) {//ищем существует ли в базе Тип клиента с таким именем
        if (err) {//если вышла ошибка при поиске, то вернем это сообщ.
            return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: 'Что-то пошла не так. Пробуйте еще раз пж. Ошибка: '+err, typeClient: typeClient});
        }
        if(!typeClient) {
            var newTypeClient = new TypeClient(); //создаем экземпляр
            newTypeClient.name = req.body.name;

            newTypeClient.save(function(err) {
                if (err) { //если ошибка при сохр то вернем его
                    return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: err, typeClient: typeClient});
                } else {
                    return index(res, req, null, 'Успешно добавлено.', null);
                }
            });
        } else {
            return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: 'Тип с таким именем уже существует.', typeClient: typeClient});
        }
    });
});

router.get('/edit/:id', function(req, res, next) {
    var typeClientId = req.params.id;
    if (!typeClientId || !ObjectId.isValid(typeClientId)) res.redirect('/type-clients/'); //если пустое id

    TypeClient.findById(typeClientId, function(err, typeClient) {
        if (err) {
            return index(res, req, err, null, null);
        }
        if(typeClient) {
            return res.render('type-clients/edit', {csrfToken: req.csrfToken(), typeClient: typeClient});
        }
    });
});

router.post('/edit', function (req, res, next) {
   var typeClientId = req.body._id;
    if (!typeClientId || !ObjectId.isValid(typeClientId)) {//если пустое id либо формат неправильный
        return index(res, req, 'Что-то пошло не так!', null, null);
    }

    TypeClient.findOne({name: req.body.name}, function(err, typeClient) {//ищем существует ли в базе Тип клиента с таким именем
        if (err) {//если вышла ошибка при поиске, то вернем это сообщ.
            return res.render('type-clients/create', {csrfToken: req.csrfToken(), errorMessages: 'Что-то пошла не так. Пробуйте еще раз пж. Ошибка: '+err, typeClient: typeClient});
        }
        if(!typeClient) {
            TypeClient.findById(typeClientId, function(err, typeClient) { //находим ы БД данный элемент
                if (err) { //если ошибка вернем его
                    return res.render('type-clients/edit', {csrfToken: req.csrfToken(), typeClient: req.body, errorMessages: err});
                }
                if(!typeClient) { //если пустое тогда говорим что не найдено
                    return res.render('type-clients/edit', {csrfToken: req.csrfToken(), typeClient: req.body, errorMessages: 'Не найдено.'});
                }

                typeClient.name = req.body.name;
                typeClient.save(function(err) {//сохраняем
                    if (err) { //если есть ошибка то вернем его
                        return res.render('type-clients/edit', {csrfToken: req.csrfToken(), typeClient: req.body, errorMessages: err});
                    }
                    return index(res, req, null, 'Успешно изменена.', null);
                });
            });
        } else {
            return res.render('type-clients/edit', {csrfToken: req.csrfToken(), errorMessages: 'Тип с таким именем уже существует.', typeClient: typeClient});
        }
    });

});

router.post('/delete', function(req, res, next) {
    var typeClientId = req.body._id;
    if (!typeClientId || !ObjectId.isValid(typeClientId)) return index(res, req, 'Что-то пошло не так.', null, null);

    TypeClient.findById(typeClientId, function(err, typeClient) {
        if (err) {
            return index(res, req, err, null, null);
        }
        if (!typeClient) {
            return index(res, req, 'Не найдено', null);
        }
        typeClient.remove(function(err) {
            if (err) {
                return index(res, req, err, null, null);
            }
            return index(res, req, null, 'Успешно удалена.', null);
        });
    });
});


module.exports = router;

function index(res, req, errorMsg, successMsg, infoMsg) {
    var errorMessages = errorMsg;
    var successMessages = successMsg;
    var infoMessages = infoMsg;

    TypeClient.find(function(err, typeClients) {
        if (err) {
            errorMessages+=err;
            return res.render('type-clients/index', {
                csrfToken: req.csrfToken(),
                errorMessages: errorMessages,
                successMessages:successMessages,
                infoMessages:infoMessages,
                typeClients: null
            });
        }
        if (!typeClients || typeClients.length==0) {
            infoMessages+='Данные не найдены. ';
            return res.render('type-clients/index',  {
                csrfToken: req.csrfToken(),
                errorMessages: errorMessages,
                successMessages:successMessages,
                infoMessages: infoMessages,
                typeClients: null
            });
        }

        return res.render('type-clients/index',  {
            csrfToken: req.csrfToken(),
            errorMessages: errorMessages,
            successMessages:successMessages,
            infoMessages: infoMessages,
            typeClients: typeClients
        });
    });
}

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/users/signin');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}