$(document).ready(function() {
    $(".select2").select2({
        "language": "ru",
        "language": {
            "noResults": function() {
                return "<a href='#' onclick=\"javascript: $('.select2').select2('close')\" data-toggle='modal' data-target='#modalAdd'>" +
                        "<i class='fa fa-plus' aria-hidden='true'></i> Добавить нового" +
                    "</a>";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    });
});

