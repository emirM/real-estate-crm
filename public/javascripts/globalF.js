    function hiddenOrVisibleElements(arrHiddenQueries, arrQueries) {//первый массив запросов на элемент для hidden, второй массив наоборот
        if (arrQueries.length == 0 && arrHiddenQueries.length == 0) return;
        for(var i=0; i<arrHiddenQueries.length; i++) {
            var elem = $(arrHiddenQueries[i]);
            if (elem.length == 0) return console.log("Элемент с запросом "+arrHiddenQueries[i]+" не найдено.");

            elem[0].style.display="none";
        }

        for(var j=0; j<arrQueries.length; j++) {
            var elem = $(arrQueries[j]);
            if (elem.length == 0) return console.log("Элемент с запросом "+arrQueries[j]+" не найдено.");

            elem[0].style.display="";
        }
    }


    function clearFieldsValue(arrQueriesElems) { // приходит jquery запросы по которым ищется элемент 
        if (!arrQueriesElems) return console.log("не корректные входные параметры");
        for(var i=0; i<arrQueriesElems.length; i++) {
            var elem = $(arrQueriesElems[i]);
            if (elem.length == 0) return console.log("Элемент с запросом "+arrQueriesElems[i]+" не найдено.");

            elem.val("");//очищаем его
        }
    }

    function hiddenElemAfterTime(idEllem, seconds) {// делает невидимым элемент после указанное секунды
        var elem = $("#"+idEllem);
        if (elem.length == 0) return console.log("Элемент с id="+idEllem+" не найдено.");
        seconds = seconds || 1;
        setTimeout(function() {
            elem[0].style.display = "none";
        }, seconds*1000);
    }

    function findElem(elemQuery) { //jquery запрос
        var elem = $(elemQuery);
        if (elem.length == 0) {
            console.log("Элемент с запросом '"+elemQuery+"' не найдено.")
            return false
        }
        return elem
    }
