function setSettings(url, data, method) {
    return {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": method || "GET",
        "dataType": "json",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": data
    }
}