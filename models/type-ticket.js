var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var typeTicket = new Schema({
    name: {type: String, required: true},
    date_create: {type: Date, default: Date.now},
    date_update: {type: Date, default: Date.now},
    selectionTypeTicket: {type: Schema.Types.ObjectId, ref: "TypeClient"},
    selectionTypeTicketName: {type: String}
});

module.exports = mongoose.model("TypeTicket", typeTicket);