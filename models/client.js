var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Таблица для карточки клиента
var schema = new Schema({
    first_name: {type: String, required: [true, 'Имя не должна быть пустым']},
    last_name: {type: String, required: [true, 'Фамилия не должна быть пустым']},
    mobile_number1: {type: String, required: [true, '"Мобльный номер 1" не должна быть пустым']},
    mobile_number2: {type: String, required: false},
    fix_number: {type: Number, required: false},
    email: {type: String, required: false},
    gender: {type: Boolean, default: true},
    birthday: {type: Date, required: false},
    address: {type: String, required: false},
    facebook: {type: String, required: false},
    instagram: {type: String, required: false},
    type_client: {type: Schema.Types.ObjectId, ref: 'TypeClient'},
    type_client_name: {type: String},
    date_create: {type: Date, default: Date.now},
    date_update: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Client', schema);