var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Таблица тип клиента
var schema = new Schema({
    name: {type: String, required: [true, "Наименование обязательное поле."]},
    date_create: {type: Date, default: Date.now},
    date_update: {type: Date, default: Date.now},
    selected: {type: Boolean, default: false}//для того чтобы сохранить выбранную, использыется при выводе в селектлисте после неправильном сохр-е
});

module.exports = mongoose.model('TypeClient', schema);