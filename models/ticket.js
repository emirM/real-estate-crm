var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    type_ticket: {type: Schema.Types.ObjectId},
    type_ticketName: {type: String},
    categoryObject: {type: Schema.Types.ObjectId},
    categoryObjectName: {type: String},

    client: {type: Schema.Types.ObjectId},
    parameters: [],
    status_ticket: {type: String, default: 'открыто'},
    user_created: {type: Schema.Types.ObjectId},
    date_create: {type: Date, default: Date.now},
    date_update: {type: Date, default: Date.now},
    prioritet: {type: String, default: 'средний'}
});

module.exports = mongoose.model('Ticket', schema);