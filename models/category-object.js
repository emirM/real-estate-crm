var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategoryObject = new Schema({
    name: {type: String, required: [true, '"Наименование"-обьязательное поле.']},
    date_create: {type: Date, default: Date.now},
    date_update: {type: Date, default: Date.now},
    parameters: []
});

module.exports = mongoose.model("CategoryObject", CategoryObject);