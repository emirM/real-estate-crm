var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');

var routes = require('./routes/index');
var users = require('./routes/users');
var clients = require('./routes/client');
var tickets = require('./routes/ticket');
var typeClients = require('./routes/type-client');
var typeTickets = require('./routes/type-ticket');
var categoriesObject = require('./routes/categories-object');

var app = express();
mongoose.connect('localhost:27017/realEstate');
require('./config/passport');

// view engine setup
var hbs = expressHbs.create({
  // Specify helpers which are only registered on this instance.
  helpers: {
    ifCond: function (v1, operator, v2, options) {
      switch (operator) {
        case '==':
          if(v1 == v2) {
            return options.fn(this);
          }
          break;
        case '===':
          if(v1 === v2) {
            return options.fn(this);
          }
          break;
        case '!=':
          if(v1 !== v2) {
            return options.fn(this);
          }
          break;
        case '<':
          if(v1 < v2) {
            return options.fn(this);
          }
          break;
        case '>':
          if(v1 > v2) {
            return options.fn(this);
          }
          break;
        case '<=':
          if(v1 <= v2) {
            return options.fn(this);
          }
          break;
        case '>=':
          if(v1 >= v2) {
            return options.fn(this);
          }
          break;
      }

      return options.inverse(this);
    }
  },
  defaultLayout: 'layout',
  extname: '.hbs'
});
//app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(session({secret: 'mysupersecret', resave: false, saveUninitialized: false}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('bower_components'));

app.use(function(req, res, next) {
  res.locals.login = req.isAuthenticated();
  res.locals.session = req.session;
  next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/clients', clients);
app.use('/tickets', tickets);
app.use('/type-clients', typeClients);
app.use('/type-tickets', typeTickets);
app.use('/categories-object', categoriesObject);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
