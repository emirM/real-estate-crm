var Client = require('../models/client');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/realEstate');

var clients = [
    new Client({
        first_name: 'A',
        last_name: 'B',
        mobile_number1: '+996705888670',
        gender: true
    }),

    new Client({
        first_name: 'A2',
        last_name: 'B2',
        mobile_number1: '+996705888670',
        gender: true
    }),
    new Client({
        first_name: 'A2',
        last_name: 'B2',
        mobile_number1: '+996705888670',
        gender: true
    })
];

var done = 0;
for(var i=0; i<clients.length; i++) {
    clients[i].save(function(err, result) {
        console.log(err);
        console.log(result);
        done++;
        if(done === clients.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}